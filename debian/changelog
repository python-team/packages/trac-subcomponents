trac-subcomponents (1.3.3-2) unstable; urgency=medium

  * Bump debhelper-compat to 13

 -- Martin <debacle@debian.org>  Sun, 22 Oct 2023 08:02:06 +0000

trac-subcomponents (1.3.3-1) unstable; urgency=medium

  * New upstream version, compatible with Trac 1.6 and Python 3

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from old 11 to 12.

 -- Martin <debacle@debian.org>  Mon, 16 Oct 2023 22:08:26 +0000

trac-subcomponents (1.3.1-1) unstable; urgency=medium

  * new upstream version
  * move repo to salsa.d.o
  * bump standards and dh compat

 -- W. Martin Borgert <debacle@debian.org>  Sat, 05 May 2018 23:58:10 +0000

trac-subcomponents (1.2.0+hga86f0413121f-4) unstable; urgency=low

  * Fix UnicodeError exception in Trac query (Closes: #774424).

 -- W. Martin Borgert <debacle@debian.org>  Sat, 03 Jan 2015 00:22:41 +0000

trac-subcomponents (1.2.0+hga86f0413121f-3) unstable; urgency=low

  [minor cleanups]
  * renamed git branches according to git-bp/git-dpm standards
  * changed debian/copyright to new format
  * new policy, no changes
  * PAPT as maintainer, I hope git is accepted now

 -- W. Martin Borgert <debacle@debian.org>  Wed, 08 Oct 2014 23:10:12 +0000

trac-subcomponents (1.2.0+hga86f0413121f-2) unstable; urgency=low

  * Fix typo in debian/control.

 -- W. Martin Borgert <debacle@debian.org>  Sat, 25 Jan 2014 11:23:40 +0000

trac-subcomponents (1.2.0+hga86f0413121f-1) unstable; urgency=low

  * New upstream with clear MIT/Expat license.
  * Moved to collab-maint git.

 -- W. Martin Borgert <debacle@debian.org>  Sat, 25 Jan 2014 00:06:11 +0000

trac-subcomponents (1.2.0-1) unstable; urgency=low

  * New upstream release, targeting Trac 1.0
  [ Jakub Wilk <jwilk@debian.org> ]
  * Use canonical URIs for Vcs-* fields.

 -- W. Martin Borgert <debacle@debian.org>  Sun, 29 Sep 2013 16:24:19 +0000

trac-subcomponents (1.1.2-1) unstable; urgency=high

  * First Debian package (Closes: #702441).

 -- W. Martin Borgert <debacle@debian.org>  Sun, 17 Mar 2013 22:56:19 +0000
